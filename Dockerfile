FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN yarn --production
# If you are building your code for production
# RUN npm ci --only=production

# Remove excess cache to optimize the size of the Docker image
RUN rm -rf /usr/local/share/.cache/
RUN rm -rf /root/.cache/

EXPOSE 3000

# Bundle app source
COPY . .

CMD [ "yarn", "start" ]