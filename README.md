# DevOps Challenge
Thank you for your interest in working at Lendico! :)
This is a little project that would help us know your DevOps skills a bit better.

## Problem Statement
Your favorite dev team has developed a node.js app that needs to be built and run as a service on Kubernetes.
The application code is under [server.js](./src/server.js), go through it to see what this mysterious application does. Take into account all endpoints when you design your solution.

## Sample Topology
![topolgy](./topology.png)

## Your Task
- Build the application using Docker
- Deploy the application into Kubernetes using Helm (minikube should do)
- Create a simple pipeline to build and deploy with your favorite CICD tool
- Document everything in a nifty readme file
- Bonus: show your Terraform skills and create the database instance on your favorite cloud solution
- Commit your changes into a new branch, we look forward to your PR!


# Solution
# ________________________________________________________________
# Wiki:

Manuals:

* [Terraform](./terraform/README.md)
* [Mongo and Nginx Ingress Controller Installation ](./helm/README.md)
* [helm application installation](./k8s/README.md)

* Access the application via:

    * Create a `/etc/hosts` entry in your work macine

    ```
    % kubectl get svc
    NAME                                             TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)                      AGE
    kubernetes                                       ClusterIP      10.1.32.1     <none>          443/TCP                      19h
    lb-external-ingress-nginx-controller             LoadBalancer   10.1.60.251   34.141.90.218   80:31017/TCP,443:31730/TCP   18h
    lb-external-ingress-nginx-controller-admission   ClusterIP      10.1.60.232   <none>          443/TCP                      18h
    
    % server.example.local 34.141.90.218
    ```
    


