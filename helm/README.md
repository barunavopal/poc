# Updated Helm repo for nginx-ingress controller from Google
 ```
 $ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
 $ helm repo list
	NAME         	URL
   ingress-nginx	https://kubernetes.github.io/ingress-nginx
 $ helm upgrade --install nginx-ingress -f values-fortunica.yaml ingress-nginx/ingress-nginx
 ```

# Updated Helm repo for nginx-ingress controller from Google

```
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm install mongodb bitnami/mongodb -f values.mongodb.yaml
```
* Values of the DB and DB password is stored in kubernets secret.
  ```
  % kubectl get secret 
  NAME                                                TYPE                                  DATA   AGE
  default-secret                                      Opaque                                2      16h
  ```

* Service is defined as per below:
  ```  
  % kubectl get svc
  NAME             TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
  deployment-dev   NodePort    10.1.62.245   <none>        80:31665/TCP   2m53s
  dev-deployment   NodePort    10.1.62.83    <none>        80:32091/TCP   103m
  mongodb          ClusterIP   10.1.60.32    <none>        27017/TCP      17h
  ```

