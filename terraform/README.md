# terraform as IaC 

* VPC created in Frankfurt region
* Networks prepared for GKE cluster
* GKE cluster created

# Installation

* need GCP account and service account key for deployment
* terraform installed in work machine (~/< 0.13)
* Steps:
    * `terraform init` -> initialiting the deployment
    * `terraform plan` --> to see what are we deploying
    * `terraform apply -auto-approve` ---> will deploy the resources 

* CleanUp
    * `terraform destroy -auto-approve`


