data "google_container_engine_versions" "gkeversion" {
  region                        = var.location
  project                       = var.project
}

data "google_compute_network" "default" {
  name = "pocnetwork"
}

data "google_compute_subnetwork" "my_subnetwork" {
  name   = var.subnetwork
  region = var.region
}

resource "google_container_cluster" "cluster" {
  name                          = var.cluster_name
  location                      = var.location 
  project                       = var.project
  min_master_version            = data.google_container_engine_versions.gkeversion.latest_master_version 
  network                       = data.google_compute_network.default.self_link
  subnetwork                    = data.google_compute_subnetwork.my_subnetwork.self_link
  monitoring_service            = "monitoring.googleapis.com/kubernetes"
  logging_service               = "logging.googleapis.com/kubernetes"
  remove_default_node_pool      = true
  initial_node_count            = 1
  

  ip_allocation_policy{
    use_ip_aliases                  = true
    cluster_secondary_range_name    = var.pod_range_name
    services_secondary_range_name   = var.service_range_name
  }
  network_policy {
    enabled = true
  }
  
  maintenance_policy {
    
    daily_maintenance_window {
      start_time = "02:00"
    }
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
  addons_config {
    http_load_balancing {
      disabled = false
    }
    horizontal_pod_autoscaling {
        disabled = false
    }
  }

  private_cluster_config {
    enable_private_endpoint         = false
    enable_private_nodes            = false
    #master_ipv4_cidr_block          = var.master_cidr 
  }

  master_auth {
     username = ""
     password = ""

     client_certificate_config {
       issue_client_certificate = "false"
     }
  }
  
  master_authorized_networks_config { 
          cidr_blocks {
            cidr_block   = "0.0.0.0/0" 
            display_name = "All" 
          }
         
  }
  
}

############### GKE Cluster End #####################
############### GKE Pool Start #####################

resource "google_container_node_pool" "nodepool0" {
  cluster                           = google_container_cluster.cluster.name
  location                          = var.location
  project                           = var.project
  version                           = data.google_container_engine_versions.gkeversion.latest_node_version 
  name                              = var.node_pool_name
  node_count                        = var.node_count

  autoscaling {
    min_node_count                  = var.autoscaling_min_node_count
    max_node_count                  = var.autoscaling_max_node_count
  }

  management {
    auto_repair   = true
    auto_upgrade  = false
  }
  
  node_config {
    preemptible                     = false
    disk_size_gb                    = var.disk_size_gb
    disk_type                       = var.disk_type
    machine_type                    = var.machine_type
    service_account                 = var.service_account_email
 
    oauth_scopes = [
      "https://www.googleapis.com/auth/userinfo.email",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.full_control",
      "https://www.googleapis.com/auth/taskqueue",
      "https://www.googleapis.com/auth/bigquery",
      "https://www.googleapis.com/auth/sqlservice.admin",
      "https://www.googleapis.com/auth/datastore",
      "https://www.googleapis.com/auth/logging.admin",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/bigtable.data",
      "https://www.googleapis.com/auth/bigtable.admin",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/service.management",
      "https://www.googleapis.com/auth/logging.write",
    ]

    metadata    = {
        google-compute-enable-virtio-rng  = "true"
        disable-legacy-endpoint           = true
        disable-legacy-endpoints          = true
    }
    labels      = {
        "cluster"   = var.cluster_name
        "stage"     = var.stage
    }
  
    tags        = var.cluster_tags
  }
 
  depends_on = [google_container_cluster.cluster]
}

############### GKE Pool End #####################


######## Outputs ###########

###### Cluster endpoints ######
output "cluster_endpoint" {
  value = google_container_cluster.cluster.endpoint
}

output "ca_certificate" {
  sensitive   = true
  value       = google_container_cluster.cluster.master_auth[0].cluster_ca_certificate
}
