terraform {
  required_version          = ">= 0.12"
  
#   backend "s3" {
#     encrypt                 = true
#     bucket                  = "barunavo-default-aws-builds"
#     region                  = "eu-central-1"
#     profile                 = "terraform-state"
#     shared_credentials_file = "~/.aws/credentials"
#     key                     = "gcp/personal-barunavo/terraform.tfstate"
#     dynamodb_table          = "barunavo-terraform-locks"
#    }
}


provider "google" {
  credentials               = file("~/acc/gcp-personal-barunavo.json")
  project                   = var.project  
  region                    = "europe-west3"
  version                   = "~> 2.19.0"
}

