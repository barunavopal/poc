resource "google_compute_subnetwork" "vpc_subnet" {
  name          = var.subnetwork
  project       = var.project
  ip_cidr_range = var.cidr
  region        = var.region
  network       = var.network
  private_ip_google_access  = true
  secondary_ip_range = [
    {
      range_name    = var.pod_range_name
      ip_cidr_range = var.pod_range
    },
    {
      range_name    = var.service_range_name
      ip_cidr_range = var.service_range
    },
  ]
}
