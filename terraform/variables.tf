variable "project" {
  default = "focal-appliance-323806"
}

variable "service_account_email" {
  default = ""
}

variable "location" {
  default = "europe-west3"
}

variable "stage" {
    default = "poc"
}

variable "region" {
  default = "europe-west3"
}

variable "cluster_name" {
  default = "poc-cluster"
}

variable "cluster_zone" {
  default = ""
}

variable "network" {
  default = "pocnetwork"
}

variable "subnetwork"{
  default = "k8s-network"
}

variable "pod_range" {
  default = "10.1.64.0/19"
}
variable "service_range" {
  default = "10.1.32.0/19"
}

variable "pod_range_name" {
  default = "k8s-pods-poc"
}

variable "service_range_name" {
  default = "k8s-services-poc"
}

variable "cidr" {
  default = "10.1.1.64/27"
  
}
variable "node_pool_name" {
    default = "poc-nodepool"
}

# variable "master_cidr"{
#   default = "172.0.16.32/28"
# }


variable "initial_node_count" {
  default = 1
}

variable "node_count" {
  default = 1
}

variable "autoscaling_min_node_count" {
  default = 1
}

variable "autoscaling_max_node_count" {
  default = 1
}

variable "disk_size_gb" {
  default = 50
}

variable "disk_type" {
  default = "pd-standard"
}

variable "machine_type" {
  default = "n1-highcpu-4"
}

variable "cluster_tags" {
  default = ["poc", "assignment"]
}

